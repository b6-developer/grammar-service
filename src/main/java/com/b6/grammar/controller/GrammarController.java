package com.b6.grammar.controller;

import com.b6.grammar.model.datamodel.GeneralResponse.GeneralResponse;
import com.b6.grammar.service.grammar.behaviour.GrammarBehaviour;
import com.b6.grammar.service.response.ResponseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@RestController
@EnableWebMvc
@RequestMapping("/api/grammar/id/")
public class GrammarController {

    @Autowired
    private GrammarBehaviour grammarBehaviour ;

    @RequestMapping("/sinonim/{word}")
    public GeneralResponse findSynonym(@PathVariable(name = "word")String word) {
        String sinonimKata = grammarBehaviour.findSynonym(word);
        return ResponseFactory.sendResponse(sinonimKata);
    }



    @RequestMapping("/antonim/{word}")
    public GeneralResponse findAntonym(@PathVariable(name = "word")String word) {
        String antonimKata = grammarBehaviour.findAntonym(word);
        return ResponseFactory.sendResponse(antonimKata);

    }
}
