package com.b6.grammar.service.Lemmatizer;

import jsastrawi.morphology.DefaultLemmatizer;
import jsastrawi.morphology.Lemmatizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class IndonesianLemmatizer {

    private Set<String> dictionary = new HashSet<>();
    private Lemmatizer lemmatizer;
    private boolean isInitialize;
    private static IndonesianLemmatizer indonesianLemmatizerInstance = new IndonesianLemmatizer();
    private  String resourceDirectory = "/root-words.txt";

    private IndonesianLemmatizer() {

    }

    private  void init() {
        InputStream in = Lemmatizer.class.getResourceAsStream(resourceDirectory);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                dictionary.add(line);
                lemmatizer = new DefaultLemmatizer(dictionary);
                isInitialize = true;
            }
        } catch (IOException e) {
            System.out.println("Error when readline");
        }

    }

    public  String lemmatize(String word) {
        if (!isInitialize) {
            init();
        }
        return lemmatizer.lemmatize(word);
    }

    public static IndonesianLemmatizer getInstance() {
        return  indonesianLemmatizerInstance;
    }

}
