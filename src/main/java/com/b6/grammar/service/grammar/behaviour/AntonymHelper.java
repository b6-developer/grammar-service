package com.b6.grammar.service.grammar.behaviour;

public interface AntonymHelper {
    public String findAntonym(String word);
}
