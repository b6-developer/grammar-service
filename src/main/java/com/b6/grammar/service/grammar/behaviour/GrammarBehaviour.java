package com.b6.grammar.service.grammar.behaviour;

public abstract class GrammarBehaviour {
    private SynonymHelper synonymHelper;
    private AntonymHelper antonymHelper;

    public abstract boolean isStandard(String word);

    public GrammarBehaviour(SynonymHelper synonymHelper, AntonymHelper antonymHelper) {
        this.synonymHelper = synonymHelper;
        this.antonymHelper = antonymHelper;
    }

    public String findSynonym(String word) {
        return synonymHelper.findSynonym(word);
    }

    public String findAntonym(String word) {
        return antonymHelper.findAntonym(word);
    }
    public abstract String findMeaning(String word);

}
