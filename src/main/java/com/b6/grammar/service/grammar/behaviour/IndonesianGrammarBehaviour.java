package com.b6.grammar.service.grammar.behaviour;

import com.b6.grammar.service.grammar.behaviour.indonesia.IndonesianAntonymHelper;
import com.b6.grammar.service.grammar.behaviour.indonesia.IndonesianSynonymHelper;
import org.springframework.stereotype.Service;

@Service
public class IndonesianGrammarBehaviour extends GrammarBehaviour {
    public IndonesianGrammarBehaviour() {
       super(new IndonesianSynonymHelper(), new IndonesianAntonymHelper());
    }

    @Override
    public boolean isStandard(String word) {
        return false;
    }


    @Override
    public String findMeaning(String word) {
        return null;
    }
}
