package com.b6.grammar.service.grammar.behaviour;

public interface SynonymHelper {
    public String findSynonym(String word);
}
