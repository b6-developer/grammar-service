package com.b6.grammar.service.grammar.behaviour.indonesia;

import com.b6.grammar.service.Lemmatizer.IndonesianLemmatizer;
import com.b6.grammar.service.grammar.behaviour.AntonymHelper;


import java.io.IOException;

public class IndonesianAntonymHelper implements AntonymHelper {
    private final String FAILED_RESPONSE = "Tidak ditemukan";
    private IndonesianLemmatizer lemmatizer = IndonesianLemmatizer.getInstance();
    private String responseToInput = "";
    IndonesianThesaurusParser parser = IndonesianThesaurusParser.getInstance();
    @Override
    public String findAntonym(String word) {
        try {
            String stemmedWord = lemmatizer.lemmatize(word);
            responseToInput = parser.getContentData(stemmedWord, "antonim");
        }catch (IOException e) {
            responseToInput = FAILED_RESPONSE;
        }
        return responseToInput;
    }



}
