package com.b6.grammar.service.grammar.behaviour.indonesia;

import com.b6.grammar.service.Lemmatizer.IndonesianLemmatizer;
import com.b6.grammar.service.grammar.behaviour.SynonymHelper;


import java.io.IOException;



public class IndonesianSynonymHelper implements SynonymHelper {
    private final String FAILED_RESPONSE = "Tidak ditemukan";
    private IndonesianLemmatizer lemmatizer = IndonesianLemmatizer.getInstance();
    private String responseToInput = "";
    IndonesianThesaurusParser parser = IndonesianThesaurusParser.getInstance();

    @Override
    public String findSynonym(String word) {
        try {
            String stemmedWord = lemmatizer.lemmatize(word);
            responseToInput = parser.getContentData(stemmedWord, "sinonim");
        }catch (IOException e) {
            responseToInput = FAILED_RESPONSE;
        }
        return responseToInput;
    }





}
