package com.b6.grammar.service.response;

import com.b6.grammar.model.datamodel.GeneralResponse.GeneralResponse;

import java.io.Serializable;

public class ResponseFactory {
    public static GeneralResponse createOkResponse(Serializable message) {
        GeneralResponse successResponse = new GeneralResponse();
        successResponse.setResult(message);
        successResponse.setStatus(200);
        successResponse.setMessage("success");
        return successResponse;
    }

    public static GeneralResponse createFailedResponse(int status, String message) {
        GeneralResponse failedResponse = new GeneralResponse();
        failedResponse.setStatus(status);
        failedResponse.setMessage(message);
        return failedResponse;
    }
    public static GeneralResponse sendResponse(String word) {
        if (word.equalsIgnoreCase("Tidak ditemukan")) {
            return createFailedResponse(404, word);
        }
        return createOkResponse(word);
    }
}
