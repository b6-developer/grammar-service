package com.b6.grammar.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GrammarControllerAntonymTest extends GrammarControllerTest {
    private final String ANTONYM_URI = GRAMMAR_URI + "antonim/";

    @Test
    public void getAntonimForValidBaseWord() throws Exception {
        String words = "hancur";
        mvc.perform(MockMvcRequestBuilders.get(ANTONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getAntonimForValidBaseWordWithPrefix() throws Exception {
        String words = "penghancur";
        mvc.perform(MockMvcRequestBuilders.get(ANTONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getAntonimForValidBaseWordWithSuffix() throws Exception {
        String words = "hancurkan";
        mvc.perform(MockMvcRequestBuilders.get(ANTONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getAntonimForValidBaseWordWithAffix() throws Exception {
        String words = "menghancurkan";
        mvc.perform(MockMvcRequestBuilders.get(ANTONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getAntonimWithInvalidWord() throws Exception {
        String words = "Yachiyo";
        mvc.perform(MockMvcRequestBuilders.get(ANTONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").doesNotExist()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(404));
    }
}