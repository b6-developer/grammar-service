package com.b6.grammar.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GrammarControllerSynonymTest extends GrammarControllerTest{

    private final String SYNONYM_URI = GRAMMAR_URI + "sinonim/";

    @Test
    public void getSinonimForValidBaseWord() throws Exception {
        String words = "beri";
        mvc.perform(MockMvcRequestBuilders.get(SYNONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getSinonimForValidBaseWordWithPrefix() throws Exception {
        String words = "memberi";
        mvc.perform(MockMvcRequestBuilders.get(SYNONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getSinonimForValidBaseWordWithSuffix() throws Exception {
        String words = "berikan";
        mvc.perform(MockMvcRequestBuilders.get(SYNONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getSinonimForValidBaseWordWithAffix() throws Exception {
        String words = "memberikan";
        mvc.perform(MockMvcRequestBuilders.get(SYNONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").exists()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Result").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(200));
    }

    @Test
    public void getSinonimWithInvalidWord() throws Exception {
        String words = "tamamo";
        mvc.perform(MockMvcRequestBuilders.get(SYNONYM_URI+words))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect((MockMvcResultMatchers.jsonPath("$.Result").doesNotExist()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(404));
    }
}