package com.b6.grammar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
public class GrammarControllerTest  {
    @Autowired
    protected MockMvc mvc;
    protected final String GRAMMAR_URI ="/api/grammar/id/";
}