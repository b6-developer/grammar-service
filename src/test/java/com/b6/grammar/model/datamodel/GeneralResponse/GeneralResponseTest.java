package com.b6.grammar.model.datamodel.GeneralResponse;

import org.junit.Test;

import static org.junit.Assert.*;

public class GeneralResponseTest {

    @Test
    public void generalResponseCreationSuccess() {
        GeneralResponse generalResponse = new GeneralResponse();
        assertNotNull(generalResponse);
    }

    @Test
    public void generalResponseSetterGetterWorking() {
        GeneralResponse generalResponse = new GeneralResponse();
        generalResponse.setStatus(200);
        generalResponse.setMessage("success");
        generalResponse.setResult("Smiling");

        assertEquals(200, generalResponse.getStatus());
        assertEquals("success", generalResponse.getMessage());
        assertEquals("Smiling", generalResponse.getResult());
    }

}