package com.b6.grammar.service.Lemmatizer;

import org.junit.Test;

import static org.junit.Assert.*;

public class IndonesianLemmatizerTest {
    IndonesianLemmatizer indonesianLemmatizer = IndonesianLemmatizer.getInstance();

    @Test
    public void lemmatizerCanLemmatizeWordWithValidInput() {
        assertEquals("suara", indonesianLemmatizer.lemmatize("menyuarakan"));
        assertEquals("makan", indonesianLemmatizer.lemmatize("memakan"));
    }


}