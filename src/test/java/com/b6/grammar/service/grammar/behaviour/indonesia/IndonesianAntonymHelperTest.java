package com.b6.grammar.service.grammar.behaviour.indonesia;

import com.b6.grammar.service.grammar.behaviour.AntonymHelper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndonesianAntonymHelperTest {

    private final String FAILED_RESPONSE = "Tidak ditemukan";
    private AntonymHelper helper;

    private String[] parseSynonyms(String synonyms) {
        return synonyms.split(", ");
    }

    @Before
    public void setUp() throws Exception {
        helper = new IndonesianAntonymHelper();
    }

    @Test
    public void testSynonymForValidBaseWord() {
        String antonyms = helper.findAntonym("hancur");
        assertNotEquals(FAILED_RESPONSE, antonyms);
        assertTrue(parseSynonyms(antonyms).length > 0);
    }

    @Test
    public void testSynonymForValidBaseWordWithPrefix() {
        String antonyms = helper.findAntonym("penghancur");
        assertNotEquals(FAILED_RESPONSE, antonyms);
        assertTrue(parseSynonyms(antonyms).length > 0);
    }

    @Test
    public void testSynonymForValidBaseWordWithSuffix() {
        String antonyms = helper.findAntonym("hancurkan");
        assertNotEquals(FAILED_RESPONSE, antonyms);
        assertTrue(parseSynonyms(antonyms).length > 0);
    }

    @Test
    public void testSynonymForValidBaseWordWithPrefixAndSuffix() {
        String antonyms = helper.findAntonym("menghancurkan");
        assertNotEquals(FAILED_RESPONSE, antonyms);
        assertTrue(parseSynonyms(antonyms).length > 0);
    }

    @Test
    public void testSynonymForInvalidWord() {
        String antonyms = helper.findAntonym("jeger");
        assertEquals(FAILED_RESPONSE, antonyms);
    }
}