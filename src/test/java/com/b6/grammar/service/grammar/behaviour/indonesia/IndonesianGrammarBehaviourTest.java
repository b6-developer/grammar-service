package com.b6.grammar.service.grammar.behaviour.indonesia;

import com.b6.grammar.service.grammar.behaviour.GrammarBehaviour;
import com.b6.grammar.service.grammar.behaviour.IndonesianGrammarBehaviour;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndonesianGrammarBehaviourTest {
    GrammarBehaviour behaviour = new IndonesianGrammarBehaviour();
    private final String FAILED_REPONSE = "Tidak ditemukan";

    @Test
    public void grammarBehaviourCanReturnCorrectResponseWithValidInput() {
        //Sinonim
        assertNotEquals(FAILED_REPONSE, behaviour.findSynonym("tenang"));
        assertNotEquals(FAILED_REPONSE, behaviour.findSynonym("senyum"));
        assertNotEquals(FAILED_REPONSE, behaviour.findSynonym("kuat"));

        //Antonim
        assertNotEquals(FAILED_REPONSE, behaviour.findAntonym("tenang"));
        assertNotEquals(FAILED_REPONSE, behaviour.findAntonym("jahat"));
        assertNotEquals(FAILED_REPONSE, behaviour.findAntonym("berani"));
    }

    @Test
    public void grammarBehaviourReturnFailedResponseWhenInvalidInput() {
        //Sinonim
        assertEquals(FAILED_REPONSE, behaviour.findSynonym("knowledge"));
        assertEquals(FAILED_REPONSE, behaviour.findSynonym("table"));
        assertEquals(FAILED_REPONSE, behaviour.findSynonym("Anchor"));

        //Antonim
        assertEquals(FAILED_REPONSE, behaviour.findAntonym("knowledge"));
        assertEquals(FAILED_REPONSE, behaviour.findAntonym("table"));
        assertEquals(FAILED_REPONSE, behaviour.findAntonym("Anchor"));
    }

    @Test
    public void grammarBehaviourReturnCorrectResponseWithRapidValidInput() {
        for(int count=0; count < 10; count++) {
            assertNotEquals(FAILED_REPONSE, behaviour.findSynonym("menyuarakan"));
        }
        for (int count=0; count<10;count++) {
            assertNotEquals(FAILED_REPONSE, behaviour.findAntonym("berani"));
        }
    }

}