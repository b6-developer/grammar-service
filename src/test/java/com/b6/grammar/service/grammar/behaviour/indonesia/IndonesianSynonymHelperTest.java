package com.b6.grammar.service.grammar.behaviour.indonesia;

import com.b6.grammar.service.grammar.behaviour.SynonymHelper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndonesianSynonymHelperTest {
    private final String FAILED_RESPONSE = "Tidak ditemukan";
    private SynonymHelper helper;

    private String[] parseSynonyms(String synonyms) {
        return synonyms.split(", ");
    }

    @Before
    public void setUp() throws Exception {
        helper = new IndonesianSynonymHelper();
    }

    @Test
    public void testSynonymForValidBaseWord() {
        String synonyms = helper.findSynonym("beri");
        assertNotEquals(FAILED_RESPONSE, synonyms);
        assertTrue(parseSynonyms(synonyms).length > 0);
    }

    @Test
    public void testSynonymForValidBaseWordWithPrefix() {
        String synonyms = helper.findSynonym("memberi");
        assertNotEquals(FAILED_RESPONSE, synonyms);
        assertTrue(parseSynonyms(synonyms).length > 0);
    }

    @Test
    public void testSynonymForValidBaseWordWithSuffix() {
        String synonyms = helper.findSynonym("berikan");
        assertNotEquals(FAILED_RESPONSE, synonyms);
        assertTrue(parseSynonyms(synonyms).length > 0);
    }

    @Test
    public void testSynonymForValidBaseWordWithPrefixAndSuffix() {
        String synonyms = helper.findSynonym("memberikan");
        assertNotEquals(FAILED_RESPONSE, synonyms);
        assertTrue(parseSynonyms(synonyms).length > 0);
    }

    @Test
    public void testSynonymForInvalidWord() {
        String synonyms = helper.findSynonym("duarr");
        assertEquals(FAILED_RESPONSE, synonyms);
    }

}