package com.b6.grammar.service.response;

import com.b6.grammar.model.datamodel.GeneralResponse.GeneralResponse;
import org.junit.Test;

import static org.junit.Assert.*;

public class ResponseFactoryTest {

    @Test
    public void responseFactoryCanCreateSuccessResponse() {
        GeneralResponse successResponse = ResponseFactory.createOkResponse("Tenang, damai");
        assertEquals(200, successResponse.getStatus());
        assertEquals("success", successResponse.getMessage());
        assertEquals("Tenang, damai", successResponse.getResult());
    }

    @Test
    public void responseFactoryCanCreateFailedResponse() {
        GeneralResponse failedResponse = ResponseFactory.createFailedResponse(404, "Tidak ditemukan");
        assertEquals(404, failedResponse.getStatus());
        assertEquals("Tidak ditemukan", failedResponse.getMessage());
        assertNull(failedResponse.getResult());
    }

    @Test
    public void responseFactoryCanSendCorrectResponse() {
        String validWord = "Marah, berang";
        GeneralResponse validResponse = ResponseFactory.sendResponse(validWord);
        assertEquals(200, validResponse.getStatus());
        assertEquals("success", validResponse.getMessage());
        assertEquals("Marah, berang", validResponse.getResult());

        String invalidWord = "Tidak ditemukan";
        GeneralResponse failedResponse = ResponseFactory.sendResponse(invalidWord);
        assertEquals(404, failedResponse.getStatus());
        assertEquals("Tidak ditemukan", failedResponse.getMessage());
        assertNull(failedResponse.getResult());
    }



}